<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

#Route::get('/home', 'HomeController@index')->name('home');

// Route::group(['middleware' => ['auth']], function() {
    
// });
Route::group(['as' => 'admin.', 'prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => ['auth']], function() {
    Route::get('/dash','AdminController@index')->name('admin_home');
    Route::resource('roles','RoleController');
    Route::resource('users','UserController');
    Route::resource('products','ProductController');
    Route::resource('pages','PageController');
    Route::resource('posts','PostController');
    Route::resource('categories','CategoryController');
    Route::resource('media','MediaController');
    Route::resource('tags','TagController');
    route::get('profile/','ProfileController@show')->name('profile');
    route::post('profile/','ProfileController@update')->name('profile.update');
    Route::get('setting/general/','GeneralSettingController@general')->name('setting.general');
    route::post('setting/general/','GeneralSettingController@store')->name('setting.update');
    route::get('setting/contact/','GeneralSettingController@contact')->name('setting.contact');
    route::post('setting/contact/','GeneralSettingController@store')->name('setting.updateContact');
    route::get('setting/email/','GeneralSettingController@email')->name('setting.email');
    route::post('setting/email/','GeneralSettingController@store')->name('setting.emailUpdate');
    route::get('setting/google/','GeneralSettingController@google')->name('setting.google');
    route::post('setting/google/','GeneralSettingController@store')->name('setting.googleUpdate');
    Route::post('ckeditor/image_upload', 'CKEditorController@upload')->name('upload');
    Route::get('ckeditor/image_browse', 'CKEditorController@browse')->name('browse');
    Route::post('commonupload','CKEditorController@common_upload')->name('common_upload');
});

Route::get('/test', function(){
    $zones = timezone_identifiers_list();

    dd(array_combine(timezone_identifiers_list(),timezone_identifiers_list()));

});
//Route::get('{slug}','PageController@all')->where('slug','([A-Za-z0-9\-\/]+)');