<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Config;
use App\Services\Setting;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('path.public', function() {
            $path = (base_path());
            return $path.'/public_html';
          });
         // dd(Setting::listLocat());
        
           
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        
    }
}
