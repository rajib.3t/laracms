<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App;
class UploadServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        App::bind('Upload',function(){
            return new App\Services\Upload;
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
