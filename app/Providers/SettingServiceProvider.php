<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App;
use Config;
use Setting;
class SettingServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        App::bind('Setting',function(){
            return new App\Services\Setting;
        });
        App::bind('userdata',function(){
            return new App\Services\Usermeta;
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        if(Setting::get('site_name')){
            Config::set('app.name', Setting::get('site_name')); 
        }
        if(Setting::get('timezone')){
            Config::set('app.timezone', Setting::get('timezone')); 
        }

        if(Setting::get('site_lang')){
            Config::set('app.locale', Setting::get('site_lang')); 
        }
    }
}
