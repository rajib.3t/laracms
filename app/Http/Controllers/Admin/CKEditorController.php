<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;
use App\Services\Upload;

class CKEditorController extends Controller
{
    public function upload(Request $request)
    {
        if($request->hasFile('upload')) {
            //get filename with extension
            $up = new Upload;
            $CKEditorFuncNum = $request->input('CKEditorFuncNum');
            $url = $up->do_upload($request->file('upload')); 
            
            $msg = 'Image successfully uploaded'; 
            $re = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";
             
            // Render HTML output 
            @header('Content-type: text/html; charset=utf-8'); 
            echo $re;
        }
    }

    public function browse(Request $request){
        $media = Post::where('post_type', 'attachment')->orderBy("id",'desc')->get();
        //dd($media->metas->toArray());
        return view('admin.support.browse',compact('media'));
    }



    public function common_upload(Request $request ){
        if($request->hasFile('file')){
            $up = new Upload;
            $url = $up->do_upload($request->file('file'));
            if($url){
                return response()->json(['status' => true, 'url'=>$url,'success' => 'Updated Successfully']);
            }
        }
    }
}
