<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;
use App\Category;
use App\Tag;
use App\Services\Slug;
use App\Services\Parents;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Post::where('post_type', 'post')->orderBy("id",'desc')->get();
        return view('admin.posts.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category = Category::first();
        $this->validate($request,[
            'name'=>'required'
        ]);
        $inputs['title'] = $request->name;
        $inputs['user_id']=Auth::user()->id;
        $s = new Slug;
        $slug = $s->createSlug($request->name);
        $inputs['post_type']='post';
        $inputs['slug']=$slug;
        $inputs['status']=1;
        $inputs['parent']=0;
        $post = Post::create($inputs);
        $post->categories()->attach($category);

        if($post){
            return redirect()->route('admin.posts.edit',$post->id)
                    ->with('success','Post created successfully.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);
        $cat = $post->categories()->get();
        $hasCats = array();
        foreach($cat as $c){
          $hasCats[]  = $c->id;
        }
        $hasTags = $post->tags()->get();
        $tags = Tag::all();
        $parents = Parents::get_parent('post',$id);
        $categories = Category::all();
        return view('admin.posts.edit', compact('post','parents','categories','hasCats','hasTags','tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request);
        $post = Post::findOrFail($id);
        //dd($request);
        $this->validate($request,[
            'title'=>'required'
        ]);
        $inputs['post_type']= 'post';
        $inputs['title']= $request->title;
        $parent = $request->parent;
        if($post->parent != $parent){
            $s = new Slug;
            $slug = $s->createSlug($request->title,$id,$parent);
            $inputs['slug']=$slug;
            $inputs['parent']=$parent;
        } 
        $inputs['content'] = $request->content;
        $result = $post->update($inputs);
        if($request->url_feature_image != null){
            update_post_meta($id,'faeture_image',$request->url_feature_image);
        }
        if($request->category){
            $post->categories()->sync($request->category);
        }
        if($request->tags){

            $post->tags()->sync($request->tags);
        }
        //dd('test');
        if($result){
            return redirect()->route('admin.posts.edit',$id)
                    ->with('success','Post edited successfully.');
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
