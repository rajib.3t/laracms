<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use Userdata;
class ProfileController extends Controller
{
    public function show()
    {
       $id =  Auth::user()->id;
       $user = User::find($id);
       return view('admin.users.profile',compact('user'));
    }
    public function update(Request $request){
        $id =  Auth::user()->id;
        $user = User::find($id);
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
            'password' => 'same:confirm-password',
            
        ]);
        $input = $request->all();
        if(!empty($input['password'])){ 
            $input['password'] = Hash::make($input['password']);
        }else{
            $input = array_except($input,array('password'));    
        }
        if(!empty($input['url_profile_picture'])){
            Userdata::set($id,'url_profile_picture',$input['url_profile_picture']);
        }
        $result = $user->update($input);
        if($result){
            return redirect()->route('admin.profile')
                        ->with('success','Profile  successfully.');
        } 
    }
}
