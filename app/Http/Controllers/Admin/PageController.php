<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Post;
use App\PostMeta;
use Illuminate\Http\Request;
use App\Services\Parents;
use App\Services\PostMetaService;
use App\Services\Slug;
use App\Services\Templates;
use App\Category;
class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $data = Post::where('post_type', 'page')->orderBy("id",'desc')->get();
        return view('admin.pages.list',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
          ]);

          $inputs['title'] = $request->name;
          $inputs['user_id']=Auth::user()->id;
          $s = new Slug;
          $slug = $s->createSlug($request->name);
            
            $inputs['post_type']='page';
            $inputs['slug']=$slug;
            $inputs['status']=1;
            $inputs['parent']=0;
            $result = Post::create($inputs);
            $meta_update = $result->metas()->create(
                array(
                    
                    'meta_key'=>'templates',
                    'meta_value'=>'page_templates.default'
                )
                );
           // print_r($result);exit;
            if($result){
                return redirect()->route('admin.pages.edit',$result->id)
                        ->with('success','Page created successfully.');
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // update_post_meta($id,'test1','abc1');
        // update_post_meta($id,'test3','xyzaa11');
        //echo  get_postmeta($id,'faeture_image',true);
        
        // $parents = Parents::get_parent('page',$id);
        // print_r($parents);

        $post=Post::find($id)->metas()->where('meta_key','templates')->first();
        dd($post);
         
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $templates = Templates::get_template('page');
        $parents = Parents::get_parent('page',$id);
        
        //$feture_image = get_postmeta($id,'faeture_image', true); exit;
        $page = Post::find($id);
        return view('admin.pages.edit', compact('page','templates','parents'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $page = Post::findOrFail($id);
        //dd($page);
        $this->validate($request, [
            'title' => 'required',

          ]);
        $inputs['post_type']= 'page';
        $inputs['title']= $request->title;
        $parent = $request->parent;
        if($page->parent != $parent){
            $s = new Slug;
            $slug = $s->createSlug($request->title,$id,$parent);
            $inputs['slug']=$slug;
            $inputs['parent']=$parent;
        } 
        $inputs['content'] = $request->content;
       // print_r($inputs);exit;
        $result = $page->update($inputs);
        //dd($result);
        // $page->metas()->update([
        //     'meta_key'=>'templates',
        //     'meta_value'=>$request->page_template
        // ]);
         update_post_meta($id,'templates',$request->page_template);
        if($request->url_feature_image != null){
            update_post_meta($id,'faeture_image',$request->url_feature_image);
        }

        if($result){
            return redirect()->route('admin.pages.edit',$id)
                    ->with('success','Page edited successfully.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
