<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\GeneralSettingRequest;
use Setting;
class GeneralSettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function general()
    {
        return view('admin.setting.general');
    }

    public function contact()
    {
        return view('admin.setting.contact');
    }

    public function email(){
        return view('admin.setting.email');
    }

    public function google(){
        return view('admin.setting.google');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GeneralSettingRequest $request)
    {
        $route = $request->route()->getName();
        //dd($route);
        switch($route){
            case 'admin.setting.update':
                {
                    $site_name = $request->site_name;
                    Setting::update('site_name',$site_name);
                    if($request->tag_line !=''){
                        Setting::update('tag_line',$request->tag_line);
                    }

                    Setting::update('site_lang',$request->language );
                    Setting::update('timezone',$request->timezone );
                    if($request->url_site_logo !=''){
                        Setting::update('site_logo', $request->url_site_logo);
                    }
                    if($request->url_site_favicon !=''){
                        Setting::update('site_favicon', $request->url_site_favicon);
                    }
                    return redirect()->route('admin.setting.general')
                        ->with('success','Setting  successfully.');
                }
            case 'admin.setting.updateContact':
                {
                    Setting::update('site_email',$request->email);
                    Setting::update('site_phone',$request->phone);
                    Setting::update('address_1',$request->address_1);
                    if($request->address_1 !=''){
                    Setting::update('address_2',$request->address_2);
                    }
                    Setting::update('city',$request->city);
                    Setting::update('state',$request->state);
                    Setting::update('country',$request->country);
                    Setting::update('postcode',$request->postcode);
                    return redirect()->route('admin.setting.contact')
                        ->with('success','Setting  successfully.');
                }
            case 'admin.setting.emailUpdate':
                {
                    $res = Setting::update('send_to_emails',$request->emails);
                    if($request->cc_email !=''){
                        Setting::update('send_cc_emails',$request->cc_email);
                    }
                    if($request->bcc_email !=''){
                        Setting::update('send_bcc_emails',$request->bcc_email);
                    }

                    if($res){
                        return redirect()->route('admin.setting.email')
                        ->with('success','Setting  successfully.');
                    }
                }
            case 'admin.setting.googleUpdate':
                {
                    $res = Setting::update('recaptcha_sitekey',$request->sitekey);
                    $res = Setting::update('recaptcha_secretkey',$request->secretkey);
                    $res = Setting::update('google_map_api_key',$request->map_api_key);
                    $res = Setting::update('google_analytics',$request->google_analytics);
                    $res = Setting::update('google_tag_head',$request->google_tag_head);
                    $res = Setting::update('google_tag_body',$request->google_tag_body);

                    if($res){
                        return redirect()->route('admin.setting.google')
                        ->with('success','Setting  successfully.');
                    }
                }    
        }
        
    }

    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
