<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MediaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        

        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                $files = count($this->file('files'));
                for($i=0; $i < $files; $i++) {
                    $rules['files.' . $i] = 'required|mimes:txt,jpg,jpeg,png,ico,gif,pdf,doc,docx,ppt,pptx,pps,ppsx,odt,xls,xlsx,key,zip,tar,gz,rar,mp3,m4a,ogg,wav,mp4,m4v,mov,wmv,avi,mpg,ogv,3gp,3g2|max:2000';
                }
                return $rules;
            }
            default:break;
        }
    }

    public function messages()
    {
        return [ 
            'mimes' => 'Please select among these extension (txt,jpg,jpeg,png,ico,gif,pdf,doc,docx,ppt,pptx,pps,ppsx,odt,xls,xlsx,key,zip,tar,gz,rar,mp3,m4a,ogg,wav,mp4,m4v,mov,wmv,avi,mpg,ogv,3gp,3g2)',
        ];
    }


}
