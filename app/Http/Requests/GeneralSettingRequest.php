<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Route;
class GeneralSettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $route = Route::currentRouteName();
        
        switch($route){
            case 'admin.setting.update':
                {
                    return [
                        'site_name'=>'required',
                        'timezone'=>'required',
                        'language'=>'required'
                    ];
                }
                case 'admin.setting.updateContact':
                {
                    return[
                        'email'=>'required|email',
                        'phone'=>'required',
                        'address_1'=>'required',
                        'city'=>'required',
                        'state'=>'required',
                        'country'=>'required',
                        'postcode'=>'required',
                    ];
                }
                case 'admin.setting.emailUpdate':
                    {
                        return[
                            'emails'=>'required'
                        ];
                        
                    }
                default:{
                    return [

                    ];
                }

        }
        
    }

    public function messages()
    {
        return [ 
           
        ];
    }
}
