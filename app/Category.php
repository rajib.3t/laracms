<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Post;
class Category extends Model
{
    protected $fillable = ['parent','name','slug','description'];

    public function posts(){
        return $this->belongToMany(Post::class,'post_category_relationships','category_id','post_id')->withTinestamps();
    }

}
