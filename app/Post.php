<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\PostMeta;
use App\Category;
use App\Tag;
class Post extends Model
{
    
    protected $guarded = ['ID','updated_at'];
    
    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function metas(){
        return $this->hasMany(PostMeta::class,'post_id','id');
    }

    public function categories(){
        return $this->belongsToMany(Category::class,'post_category_relationships','post_id','category_id')->withTimestamps();
    }

    public function tags(){
        return $this->belongsToMany(Tag::class,'post_tag_relationships','post_id','tag_id')->withTimestamps();
    }

}
