<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostCategoryRelationship extends Model
{
    protected $guarded = ['id'];
}
