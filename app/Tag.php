<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Post;

class Tag extends Model
{
    protected $fillable = ['name','slug','description'];
    protected $guarded = [
        'created_at',
        'updated_at'
    ];
    public function posts(){
        return $this->belongToMany(Post::class,'post_tag_relationships','tag_id','post_id');
    }
}
