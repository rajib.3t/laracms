<?php
namespace App\Services;
use App\UserMeta as Userdata;
class Usermeta{
    public function get($user_id, $meta_key){
        $data = Userdata::where('user_id',$user_id)->where('meta_key',$meta_key)->first();
        if($data){
            return $data->meta_value;
        }else{
            return false;
        }
        return false;
    }


    public function set($user_id, $meta_key,$meta_value){
        $data = Userdata::where('user_id',$user_id)->where('meta_key',$meta_key)->first();
        if($data){
            $inputs = array(
                //'meta_key'=>$meta_key,
                'meta_value'=>$meta_value
            );
            $um = $data->update($inputs);
            return $um;
        }else{
            $inputs = array(
                'user_id'=>$user_id,
                'meta_key'=>$meta_key,
                'meta_value'=>$meta_value
            );
            $um = Userdata::create($inputs);
            return $um;
        }

        return false;
    }
}