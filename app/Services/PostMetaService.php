<?php

namespace App\Services;
use App\Post;
use App\PostMeta;
class PostMetaService{
    public static function update($post_id, $meta_key, $meta_value){
        $post = Post::find($post_id);
        $meta_key_exist = $post->metas()->where('meta_key',$meta_key)->first();
        if($meta_key_exist){
            $postmeta=PostMeta::where('post_id',$post_id)->where('meta_key',$meta_key)->get()->first();
            
            $inputs = array(
                //'meta_key'=>$meta_key,
                'meta_value'=>$meta_value
            );
           $pm = $postmeta->update($inputs);
        return ($pm);
        }else{
            $pm = $post->metas()->create([
                'meta_key'=>$meta_key,
                'meta_value'=>$meta_value
            ]);
            return ($pm);
        }

        return FALSE;
    }

    public static function get($post_id, $meta_key, $one = TRUE){
        $post = Post::find($post_id);
        if($one == true){
            $meta_key_exist = $post->metas()->where('meta_key',$meta_key)->first();
            
            if($meta_key_exist){
                return $meta_key_exist->meta_value;
            }else{
                return false;
            }
        }

        return FALSE;
    }
}