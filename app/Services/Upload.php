<?php

namespace App\Services;
use Illuminate\Support\Facades\Auth;
use App\Post;
use App\PostMeta;
use App\Services\Slug;
class Upload{

    public function do_upload($file){
      
        $filenamewithextension = $file->getClientOriginalName();
      
            //get filename without extension
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
      
            //get file extension
            $extension = $file->getClientOriginalExtension();
      
            //filename to store
            $f = $filename.'_'.time();
            $filenametostore = $f.'.'.$extension;
            $s = new Slug;
            $slug = $s->createSlug($f);
            //Upload File
            $file->storeAs('public/uploads', $filenametostore);
            $url = asset('storage/uploads/'.$filenametostore);
            $attach = array(
                'title'=>$filenametostore,
                'post_type'=>'attachment',
                'slug'=>$slug,
                'status'=>1,
                'user_id'=>Auth::user()->id
            );
            $media = Post::create($attach);
            $meats = array(
                'meta_key'=>'url',
                'meta_value'=>$url
            );
            $media->metas()->create($meats);
            return  $url;
    }



    public static function go_upload($name, $label, $url = null){
     
        $attachments = Post::where('post_type','attachment')->orderBy("ID",'desc')->get();
        $images = '';
        foreach($attachments as $key =>$a){
          foreach($a->metas as $u){
            if($u->meta_key == 'url'){
              $images .='<div class="col-xl-3 col-md-6 food1">
                    <div class="img-data">
                    <img src="'.$u->meta_value.'" class="img-rounded">
                    </div>
                    <input type="button" data-url="'.$u->meta_value.'" data-push="'.$name.'" onclick="selectImage(this)" value="Select File" />
                </div>';
            }
          }

            
        }
        $class = 'rm-upload-'.$name;
         
        if($url){
          $class = 'rm-remove-'.$name;
          $label = 'Remove Image';
          
        }
        $str = '<a href="javascript:void(0)" data-name="'.$name.'" data-label="'.$label.'" data-modal="'.$name.'Modal" onclick="open_modal(this)" class="'.$class.'"  >'.$label.'</a>';
        $str.='<input type="hidden" name ="url_'.$name.'" value="'.$url.'">';
        $str .='<div class="food1"><div class="img-data">
                  <img class="img-res load-'.$name.'" src="'.$url.'" class="img-rounded">
                  </div></div>';
        $str .='<!-- Modal -->
        <div id="'.$name.'Modal" class="modal fade" role="dialog">
          <div class="modal-dialog modal-lg">
        
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
              <h6 class="modal-title">Modal Header</h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                
              </div>
              <div class="modal-body">
                    <ul class="nav nav-tabs" id="myTab-'.$name.'" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="add-tab-'.$name.'" data-toggle="tab" href="#add-'.$name.'" role="tab" aria-controls="add-'.$name.'" aria-selected="true">Upload</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="select-tab-'.$name.'" data-toggle="tab" href="#select-'.$name.'" role="tab" aria-controls="select-'.$name.'" aria-selected="false">Select</a>
                    </li>
                   
                    </ul>
                    <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="add-'.$name.'" role="tabpanel" aria-labelledby="home-tab">
                    
                    <input type="hidden" name="token-'.$name.'" value="'.csrf_token().'">
                    <div class="form-group">
                        
                        <input type="file" class="form-control-file" name="upload-'.$name.'" >
                    </div>
                    <input type="submit" data-push="'.$name.'" class="btn btn-primary upload-form-s" value="Submit">
                    
                    </div>
                    <div class="tab-pane fade" id="select-'.$name.'" role="tabpanel" aria-labelledby="select-'.$name.'">
                    <div class="row">
                    '.$images.'
                    </div>
                    </div>
                    
                    </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
        
          </div>
        </div>';
        return $str;
    }
    
}
