<?php

namespace App\Services;

use App\Post;
use App\Category;
use App\Tag;
class Slug
{
    /**
     * @param $title
     * @param int $id
     * @return string
     * @throws \Exception
     */
    public function createSlug($title, $id = 0, $parent = 0)
    {
        // Normalize the title
        $slug = str_slug($title);

        // Get any that could possibly be related.
        // This cuts the queries down by doing it once.
        $allSlugs = $this->getRelatedSlugs($slug, $id, $parent);

        // If we haven't used it before then we are all good.
        if (! $allSlugs->contains('slug', $slug)){
            return $slug;
        }

        // Just append numbers like a savage until we find not used.
        for ($i = 1; $i <= 10; $i++) {
            $newSlug = $slug.'-'.$i;
            if (! $allSlugs->contains('slug', $newSlug)) {
                return $newSlug;
            }
        }

        throw new \Exception('Can not create a unique slug');
    }

    protected function getRelatedSlugs($slug, $id = 0, $parent=0)
    {
        return Post::select('slug')->where('slug', 'like', $slug.'%')
            ->where('id', '<>', $id)
            ->where('parent',$parent)
            ->get();
    }



    public  function createurl($id){
        $str = $this->getRelatedSlug($id);
        
        $slugArr = explode('/',$str);
        $revSlugArr = array_reverse(array_filter($slugArr));
        return implode('/',$revSlugArr);
    }

    protected function getRelatedSlug($id){
        $slug = '';
        $p = Post::find($id);
        $slug .=$p->slug."/";
        $parent = $p->parent;
        if($parent != 0){
            $slug .=$this->getRelatedSlug($parent);
        }
        
        return $slug;
    }


    public function createCatslug($title, $id, $parent){
        $slug = str_slug($title);

        // Get any that could possibly be related.
        // This cuts the queries down by doing it once.
        $allSlugs = $this->getRelatedCatSlug($slug, $id, $parent);

        // If we haven't used it before then we are all good.
        if (! $allSlugs->contains('slug', $slug)){
            return $slug;
        }

        // Just append numbers like a savage until we find not used.
        for ($i = 1; $i <= 10; $i++) {
            $newSlug = $slug.'-'.$i;
            if (! $allSlugs->contains('slug', $newSlug)) {
                return $newSlug;
            }
        }

        throw new \Exception('Can not create a unique slug');

    }


    protected function getRelatedCatSlug($slug, $id, $parent){
        return Category::select('slug')->where('slug','like',$slug.'%')
        ->where('id', '<>', $id)
        ->where('parent',$parent)
        ->get();
    }

    public function createTagslug($title, $id = 0){
        $slug = str_slug($title);

        // Get any that could possibly be related.
        // This cuts the queries down by doing it once.
        $allSlugs = $this->getRelatedTagSlug($slug, $id=0);

        // If we haven't used it before then we are all good.
        if (! $allSlugs->contains('slug', $slug)){
            return $slug;
        }

        // Just append numbers like a savage until we find not used.
        for ($i = 1; $i <= 10; $i++) {
            $newSlug = $slug.'-'.$i;
            if (! $allSlugs->contains('slug', $newSlug)) {
                return $newSlug;
            }
        }

        throw new \Exception('Can not create a unique slug');

    }


    protected function getRelatedTagSlug($slug, $id = 0){
        return Tag::select('slug')->where('slug','like',$slug.'%')
        ->where('id', '<>', $id)
        ->get();
    }


}