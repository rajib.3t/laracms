<?php

namespace App\Services;
use App\Post;
class Parents{

    public static function get_parent($type, $id){

                $post = Post::where([
                    ['post_type','=',$type],
                    ['id','<>',$id]
                    ])->get();
       

        $p = array(0=>'No Parent');
        foreach ($post as $key => $value) {
            $p[$value->id]=$value->title;
        }
        return $p;
    }
}