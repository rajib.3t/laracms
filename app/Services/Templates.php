<?php
namespace App\Services;

class Templates{
    public static function get_template($type){
        $resDir = resource_path().'/views';
        switch($type){
            case 'page':
                $dir = $resDir.'/page_templates';
                $page = 'page_templates';
            break;
        }
        $list = scandir($dir);
        $list = array_diff($list, array('.', '..'));
        $templates = array();
        foreach($list as $l){
            $names = explode('.',$l);
            $templates[$page.'.'.$names[0]]  = ucfirst($names[0]);
        }
        return $templates;
    }
}