<?php
use App\Services\PostMetaService;
use App\PostMeta;
use App\Services\Slug;


function geturl($id){
    $s = new Slug;
    $slug = $s->createurl($id);
    
   
    return $slug;
}

function getTop($id){
    $s = new Slug;
    $slug = $s->createurl($id);
    $slugArr = explode('/',$slug);
    array_pop($slugArr);
    if(empty($slugArr)){
        return '';
    }
    return implode('/',$slugArr);
}

function update_post_meta($post_id, $meta_key, $meta_value){
    //echo $meta_value;exit;
    return PostMetaService::update($post_id, $meta_key, $meta_value);
}

function get_postmeta($post_id, $meta_key,$one = true){
    return PostMetaService::get($post_id, $meta_key,$one = true);
}