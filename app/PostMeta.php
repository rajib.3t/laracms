<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostMeta extends Model
{
    protected $primaryKey = 'pmeta_id';
    protected $guarded = ['pmeta_id'];
    
}
