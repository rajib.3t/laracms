<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>{{ config('app.name') }} | @yield('title')</title>
        <link href="{{ asset('admin/css/styles.css') }}" rel="stylesheet" />
        <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script>
        <link href="{{ asset('admin/codemirror/codemirror.css') }}" rel="stylesheet" />
    </head>
    <body class="sb-nav-fixed">
    @include('elements.admin.header')
    @include('elements.admin.sidebar')    
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                    @yield('content')
                    </div>   
                </main>
                @include('elements.admin.sidebar') 
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="{{ asset('admin/js/scripts.js') }}"></script>
        <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script> -->
        <!-- <script src="assets/demo/chart-area-demo.js"></script>
        <script src="assets/demo/chart-bar-demo.js"></script> -->
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
        <script src="{{ asset('admin/js/datatables-demo.js') }}"></script>
        <script src="//cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
        <script src="{{ asset('admin/codemirror/codemirror.js') }}"></script>
        <script src="{{ asset('admin/codemirror/selection-pointer.js') }}"></script>
        <script src="{{ asset('admin/codemirror/xml.js') }}"></script>
        <script src="{{ asset('admin/codemirror/javascript.js') }}"></script>
        <script src="{{ asset('admin/codemirror/css.js') }}"></script>
        <script src="{{ asset('admin/codemirror/vbscript.js') }}"></script>
        <script src="{{ asset('admin/codemirror/htmlmixed.js') }}"></script>
        <script>
            $(document).ready(function(){
                if($('#page_content').length > 0){
            CKEDITOR.replace( 'page_content', {
                filebrowserUploadUrl: "{{route('admin.upload', ['_token' => csrf_token() ])}}",
                filebrowserUploadMethod: 'form',
                filebrowserBrowseUrl:"{{route('admin.browse',['_token' => csrf_token() ])}}"
            });
            CKEDITOR.on('dialogDefinition', function (ev) {
                var dialogName = ev.data.name;
                var dialogDefinition = ev.data.definition;
                console.log(dialogDefinition);
                if (dialogName == 'image') {
                    var onOk = dialogDefinition.onOk;
                    dialogDefinition.onOk=function(e){
                        var input = this.getContentElement( 'info', 'txtUrl' ),
                        imageSrcUrl = input.getValue();
                        //! Manipulate imageSrcUrl and set it
                        input.setValue( imageSrcUrl );
                        onOk && onOk.apply( this, e );
                        
                    }
                }
                });
                }
            });
                function open_modal(send){
                    var dname = $(send).data('name');
                    var label = $(send).data('label');
                    if($(send).hasClass('rm-upload-'+dname)){
                    var $modal = $(send).data('modal');
                    $("#"+$modal).modal();
                    }else if($(send).hasClass('rm-remove-'+dname)){
                        
                        $(send).removeClass('rm-remove-'+dname);
                        $(send).addClass('rm-upload-'+dname);
                        $('rm-upload-'+dname).text(label);
                        $(".load-"+dname).attr("src", ''); 
                        var urlinupt = $(send).data('name');
                        var label =$(send).data('label');
                        $('input[name="url_'+urlinupt+'"]').val('');
                    }
                }
                $(document).on('click','.upload-form-s',function(e){
                    e.preventDefault();
                    var dname = $(this).data('push');
                    var fd = new FormData();
                    var  files = $('input[name="upload-'+dname+'"]')[0].files[0];
                    var _token = $('input[name="token-'+dname+'"]').val();
                    fd.append('file',files);
                    fd.append('_token',_token);
                    $.ajax({
                        url: '{{ route('admin.common_upload') }}',
                        type: 'post',
                        data: fd,
                        contentType: false,
                        processData: false,
                        success: function (res) {
                            $('.rm-upload-'+dname).removeData('toggle');
                            $('.rm-upload-'+dname).text('Remove Image');
                            if(res.status == true){
                                $(".load-"+dname).attr("src", res.url);
                                var $modal = $('.rm-upload-'+dname).data('modal');
                                var urlinupt = $('.rm-upload-'+dname).data('name');
                                $('input[name="url_'+urlinupt+'"]').val(res.url);
                                $('#'+$modal).modal('hide');
                                $('.rm-upload-'+dname).addClass('rm-remove');
                                $('.rm-upload-'+dname).removeClass('rm-upload');
                            }
                        //document.getElementById("response").innerHTML = res; 
                        
                        }
                        });
                });

        function selectImage(url){
            var $this = url;
            var dname = $($this).data('push');
            var urlinupt = $('.rm-upload-'+dname).data('name');
            var imgsrc = $($this).data('url');
            $(".load-"+urlinupt).attr("src", imgsrc);
            var $modal = $('.rm-upload-'+dname).data('modal');
            
            $('input[name="url_'+urlinupt+'"]').val(imgsrc);
            $('#'+$modal).modal('hide');
            $('.rm-upload-'+dname).text('Remove Image');
            if($('.rm-upload-'+dname).hasClass('rm-upload-'+dname)){
                $('.rm-upload-'+dname).addClass('rm-remove-'+dname);
                $('.rm-upload-'+dname).removeClass('rm-upload-'+dname);
            }
            
        }       

        function show_new_cat(){
            $('.new-category').show(500);
        } 
        $('.cancel_cat').click(function(){
            $('.new-category').hide(500);
        });
        $('.save_cat').click(function(){
            var new_cat = $('#new_cat_name').val();
            var parent = $('#parent_cat').val();

            var flag = true;

            if(new_cat == ''){
                $('#new_cat_name').addClass('error');
                flag == false; 
            }else{
                $('#new_cat_name').removeClass('error'); 
                flag == true;
            }

            if(flag){
                $.ajax({
                    url:"{{ route('admin.categories.store') }}",
                    type:"POST",
                    data:{
                        '_token':'{!! csrf_token() !!}',
                        'parent':parent,
                        'name':new_cat
                    },
                    success:function(res){
                        if(res.status == true){
                            var myvar = '<div class="form-check">'+
                            '<input id="defaultCheck-'+res.id+'" class="form-check-input" checked="checked" name="category[]" type="checkbox" value="'+res.id+'">'+
                            '<label for="defaultCheck-'+res.id+'" class="form-check-label">'+res.name+'</label>'+
                            '</div>';
                            $('.cat-list').append(myvar);
                            $('.new-category').hide(500);
                        }
                                               
	

                    }

                })
            }
        });
        $('.save_tags').click(function(){
            var tags = $('#new_tags').val();
            var flag = true;
            if(tags == ''){
                $('#new_tags').addClass('error');
                flag == false;
            }else{
                $('#new_tags').removeClass('error');
                flag == true;
            }

            if(flag){
                $.ajax({
                    url:'{{ route('admin.tags.store') }}',
                    type:'POST',
                    data:{
                        '_token':'{!! csrf_token() !!}',
                        'name':tags
                    },
                    success:function(res){
                        console.log(res);
                    }
                });
            }
        });
        $('.choose_tags').click(function(){
            $('.unused_tag_list').show(500);
        });
        $('.add_to_tag' ).click(function(){
            var $this = $(this);
            var el = $('.tag_post_list li');
            var id = $this.data('id')
            var name = $this.data('name');
            console.log(id)
            v = [];
            if(el.length > 0){
                var flag = true;
                el.each(function(i,e){
                    //v.push($(e).find('input').val())
                    if($(e).find('input').val()==id){
                        $('#new_tags').focus();
                        //return false;
                    }else{
                        var myvar = '<li>'+name+'<span><i class="fas fa-times-circle"></i></span><input type="hidden" name="tags[]" value="'+id+'"></li>'; 
                $('.tag_post_list').append(myvar);

                    }
                    
                });
                // console.log(v)
                // console.log(jQuery.inArray(id, v) );

                // if($.inArray('"'+id+'"', v)){
                //     alert(1);
                //   $('#new_tags').focus();  
                // }else if($.inArray(id, v) == -1){
                //     alert(2)
                //     var myvar = '<li>'+name+'<span><i class="fas fa-times-circle"></i></span><input type="hidden" name="tags[]" value="'+id+'"></li>'; 
                //     $('.tag_post_list').append(myvar);  
                // }
                
            }else{
                var myvar = '<li>'+name+'<span><i class="fas fa-times-circle"></i></span><input type="hidden" name="tags[]" value="'+id+'"></li>'; 
                $('.tag_post_list').append(myvar);
            }
        });
        $(document).ready(function(){
            var mixedMode = {
                name: "htmlmixed",
                scriptTypes: [{matches: /\/x-handlebars-template|\/x-mustache/i,
                       mode: null},
                      {matches: /(text|application)\/(x-)?vb(a|script)/i,
                       mode: "vbscript"}]
                    };
            if($('#google_analytics').length > 0){
                
                var editor = CodeMirror.fromTextArea(document.getElementById("google_analytics"), {
                    mode: mixedMode,
                    lineNumbers: true,
                    selectionPointer: true
                    });
            }
            if($('#google_tag_head').length > 0){
                
                var editor = CodeMirror.fromTextArea(document.getElementById("google_tag_head"), {
                    mode: mixedMode,
                    lineNumbers: true,
                    selectionPointer: true
                    });
            }
            if($('#google_tag_body').length > 0){
                
                var editor = CodeMirror.fromTextArea(document.getElementById("google_tag_body"), {
                    mode: mixedMode,
                    lineNumbers: true,
                    selectionPointer: true
                    });
            }
        });
        
        </script>
    </body>
</html>