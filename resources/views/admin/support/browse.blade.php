<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Example: Browsing Files</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script>
        // Helper function to get parameters from the query string.
        function getUrlParam( paramName ) {
            var reParam = new RegExp( '(?:[\?&]|&)' + paramName + '=([^&]+)', 'i' );
            var match = window.location.search.match( reParam );

            return ( match && match.length > 1 ) ? match[1] : null;
        }
        // Simulate user action of selecting a file to be returned to CKEditor.
        function returnFileUrl(url) {

            var funcNum = getUrlParam( 'CKEditorFuncNum' );
            var fileUrl = url;
            window.opener.CKEDITOR.tools.callFunction( funcNum, fileUrl );
            window.close();
        }
    </script>
    <style>
    .img-data img {
    
    width:100%;
    }
    .img-data{
        height:200px;
    }
    .food1{padding:10px}
    </style>
</head>
<body>
<div class="container">
       
@foreach ($media as $key => $m)
    @foreach($m->metas as $n)
    <div class="col-xs-6 col-sm-3 food1">
    <div class="img-data">
    <img src="{{$n->meta_value}}" class="img-rounded">
    </div>
    <button onclick="returnFileUrl('{{$n->meta_value}}')">Select File</button>
    </div>
        
    @endforeach
@endforeach
        
</div>

    
</body>
</html>