@extends('layouts.master')
@section('title') Edit Page @endsection
@section('content')
<h1 class="mt-4">Edit Page</h1>
<ol class="breadcrumb mb-4">
    <li class="breadcrumb-item"><a href="{{ route('admin.admin_home') }}">Dashboard</a></li>
    <li class="breadcrumb-item active">Edit Page</li>
</ol>
@if ($message = Session::get('success'))
                            <div class="alert alert-success">
                            <p>{{ $message }}</p>
                            </div>
                            @endif
{!! Form::model($page, ['method' => 'PATCH','route' => ['admin.pages.update', $page->id]]) !!}

<div class="row">
    <div class="col-sm-8">
        <div class="form-group">
            <label for="title">{{ __('Title') }}</label>
            {!! Form::text('title', null, array('placeholder' => 'Title','class' => 'form-control','id'=>'title')) !!}

        </div>
        <div class="form-group">
        <p><strong>Permalink:</strong><span><a href="{{ url('/').'/' }}{{ geturl($page->id) }}">{{ url('/').'/' }}{{  getTop($page->id) ? getTop($page->id).'/' :'' }}<span id="editable-post-name">{{ $page->slug }}</span></a></span><span id="edit-slug-buttons"><button type="button" class="edit-slug button button-small hide-if-no-js" aria-label="Edit permalink">Edit</button></span></p>
        </div>
        <div class="form-group">
            {!! Form::label('content', 'Page Description',[ 'class' => 'control-label']) !!}
            {!! Form::textarea('content', null, ['class' => 'page_content','id'=>'page_content']) !!}
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label for="exampleFormControlSelect1">Page Templates</label>


            {!!
            Form::select('page_template',$templates,$page->metas()->where('meta_key','templates')->first()->meta_value,array('class'=>'form-control'))!!}

        </div>
        <div class="form-group">
            <label for="exampleFormControlSelect2">Parent</label>

            {!! Form::select('parent',$parents,$page->parent,array('class'=>'form-control'))!!}

        </div>
        <div class="form-group text-right">

            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
          
        
        <div class="form-group">
            {!! Upload::go_upload('feature_image','Feature Image',get_postmeta($page->id,'faeture_image', true))!!}
        </div>
    </div>

</div>
{!! Form::close() !!}
@endsection
