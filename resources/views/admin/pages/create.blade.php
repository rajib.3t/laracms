@extends('layouts.master')
@section('title') Create Post @endsection
@section('content')
<h1 class="mt-4">Create Page</h1>
<ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="{{route('admin.admin_home')}}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Create Page </li>
                        </ol>
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                            <p>{{ $message }}</p>
                            </div>
                            @endif
                        {!! Form::open(array('route' => 'admin.pages.store','method'=>'POST')) !!}
                        <div class="row">
                            <div class="col-xs-2 col-sm-2 col-md-2">
                                <div class="form-group">
                                    <strong>{{ __('Name:') }}</strong>
                                    
                                </div>
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-4">
                                <div class="form-group">
                                    
                                    {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                            <strong></strong>
                                <button type="submit" class="btn btn-primary">{{  __('Submit')}}</button>
                            </div>
                        </div>
                        {!! Form::close() !!} 
@endsection                    