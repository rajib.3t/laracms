@extends('layouts.master')
@section('title')Page List @endsection
@section('content')
<h1 class="mt-4">Create Page</h1>
<ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="{{route('admin.admin_home')}}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Page List</li>
                        </ol>
                         
                          <div class="card mb-4">
                            <div class="card-header"><i class="fas fa-table mr-1"></i>{{__('Page List')}} </div>
                            <div class="card-body">
                            @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                            <p>{{ $message }}</p>
                            </div>
                            @endif
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Name</th>
                                                <th>Slug</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                               
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>No</th>
                                                <th>Name</th>
                                                <th>Slug</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        @php ($i = 1)  
                                        @foreach ($data as $key => $page)
                                            <tr>
                                                <td>{{$i++}}</td>
                                                <td>{{ $page->title }}</td>
                                                <td>{{ $page->slug }}</td>
                                                <td>
                                                @if($page->status == '1')
                                                    {{__('publish')}}
                                                @else
                                                    {{__('draft')}}
                                                @endif
                                                </td>
                                                <td>
                                                <a class="btn btn-info" href="{{ route('admin.pages.show',$page->id) }}">Show</a>
                                                <a class="btn btn-primary" href="{{ route('admin.pages.edit',$page->id) }}">Edit</a>
                                                    {!! Form::open(['method' => 'DELETE','route' => ['admin.pages.destroy', $page->id],'style'=>'display:inline']) !!}
                                                        {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                                    {!! Form::close() !!}
                                                </td>
                                                
                                            </tr>
                                            @endforeach
                                           
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        
                        
@endsection                    