@extends('layouts.master')
@section('title'){{ __('Profile') }} @endsection
@section('content')
<h2 class="mt-4">{{ __('Profile') }}</h2>
<ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="{{route('admin.admin_home')}}">{{ __('Dashboard') }}</a></li>
                            <li class="breadcrumb-item active">{{ __('Profile') }}</li>
                        </ol>
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                            <p>{{ $message }}</p>
                            </div>
                            @endif
                        {!! Form::open(array('route' => 'admin.profile.update','method'=>'POST','files'=> true))  !!}
                        <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>{{ __('Name') }}</strong>
                                {!! Form::text('name', $user->name, array('placeholder' => 'Email','class' => 'form-control')) !!}
                                <div class="error">
                                    @if (count($errors) > 0)
                                        @foreach ($errors->get('name') as $error)
                                            <p>{{ $error }}</p>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>{{ __('Email') }}</strong>
                                {!! Form::text('email', $user->email, array('placeholder' => 'Phone','class' => 'form-control')) !!}
                                <div class="error">
                                    @if (count($errors) > 0)
                                        @foreach ($errors->get('email') as $error)
                                            <p>{{ $error }}</p>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>{{ __('Password') }}</strong>
                                {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control')) !!}
                                <div class="error">
                                    @if (count($errors) > 0)
                                        @foreach ($errors->get('password') as $error)
                                            <p>{{ $error }}</p>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>{{ __('Confirm Password') }}</strong>
                                {!! Form::password('confirm-password', array('placeholder' => 'Confirm Password','class' => 'form-control')) !!}
                                <div class="error">
                                    @if (count($errors) > 0)
                                        @foreach ($errors->get('confirm-password') as $error)
                                            <p>{{ $error }}</p>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>{{ __('Profile Picture') }}</strong>
                                {!! Upload::go_upload('profile_picture', 'Add Image',Userdata::get($user->id,'url_profile_picture')) !!}
                            </div>
                        </div>
                        
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <strong></strong>
                                <button type="submit" class="btn btn-primary">{{  __('Submit')}}</button>
                            </div>
                        </div>
                        {!! Form::close() !!} 


@endsection                                                