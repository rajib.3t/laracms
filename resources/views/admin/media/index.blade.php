@extends('layouts.master')
@section('title')Media List @endsection
@section('content')
<h1 class="mt-4">Media Page</h1>
<ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="{{route('admin.admin_home')}}">{{ __('Dashboard') }}</a></li>
                            <li class="breadcrumb-item active">{{ __('Media List') }}</li>
                        </ol>
                         
                           <div class="row">
                                @foreach ($media as $key =>$item)
                                   <div class="col-xl-3 col-md-6 media-list">
                                       <img src="{!! get_postmeta($item->id,'url') !!}" class="img-fluid">
                                   </div> 
                                @endforeach
                           </div>
                       
                        
                        
@endsection                    