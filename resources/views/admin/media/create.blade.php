@extends('layouts.master')
@section('title')Media List @endsection
@section('content')
<h1 class="mt-4">Media Add</h1>
<ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="{{route('admin.admin_home')}}">{{ __('Dashboard') }}</a></li>
                            <li class="breadcrumb-item active">{{ __('Media Add') }}</li>
                        </ol>

                        {!! Form::open(array('route' => 'admin.media.store','method'=>'POST','files'=> true))  !!}
                        
                        <div class="row">
                            <div class="col-xs-2 col-sm-2 col-md-2">
                                <div class="form-group">
                                    <strong>{{ __('File Name:') }}</strong>
                                    
                                </div>
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-4">
                                <div class="form-group">
                                    {!! Form::input('file', 'files[]', null, ['class' => 'form-control','multiple' => 'multiple']) !!}
                                </div>
                                <div class="error">
                                    @if (count($errors) > 0)
                                        @foreach ($errors->all() as $error)
                                            <p>{{ $error }}</p>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                            <strong></strong>
                                <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
                            </div>
                        </div>
                        {!! Form::close() !!} 
@endsection                        