@extends('layouts.master')
@section('title') Edit Post @endsection
@section('content')
<h1 class="mt-4">Edit Post</h1>
<ol class="breadcrumb mb-4">
    <li class="breadcrumb-item"><a href="{{ route('admin.admin_home') }}">Dashboard</a></li>
    <li class="breadcrumb-item active">Edit Post</li>
</ol>
@if ($message = Session::get('success'))
                            <div class="alert alert-success">
                            <p>{{ $message }}</p>
                            </div>
                            @endif
{!! Form::model($post, ['method' => 'PATCH','route' => ['admin.posts.update', $post->id]]) !!}

<div class="row">
    <div class="col-sm-8">
        <div class="form-group">
            <label for="title">{{ __('Title') }}</label>
            {!! Form::text('title', null, array('placeholder' => 'Title','class' => 'form-control','id'=>'title')) !!}

        </div>
        <div class="form-group">
        <p><strong>Permalink:</strong><span><a href="{{ url('/').'/' }}{{ geturl($post->id) }}">{{ url('/').'/' }}{{  getTop($post->id) ? getTop($post->id).'/' :'' }}<span id="editable-post-name">{{ $post->slug }}</span></a></span><span id="edit-slug-buttons"><button type="button" class="edit-slug button button-small hide-if-no-js" aria-label="Edit permalink">Edit</button></span></p>
        </div>
        <div class="form-group">
            {!! Form::label('content', 'Description',[ 'class' => 'control-label']) !!}
            {!! Form::textarea('content', null, ['class' => 'page_content','id'=>'page_content']) !!}
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label for="exampleFormControlSelect2">Parent</label>

            {!! Form::select('parent',$parents,$post->parent,array('class'=>'form-control'))!!}

        </div>
        <div class="form-group text-right">

            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        <div class="categories">
            <h4>{{ __('Categories') }}</h4>
            <div class="cat-list">
              @foreach ($categories as $key => $cat)
              <div class="form-check">
                {!! Form::checkbox('category[]',$cat->id,in_array($cat->id,$hasCats), array('id'=>'defaultCheck-'.$cat->id, 'class'=>'form-check-input')) !!}
                {!! Form::label('defaultCheck-'.$cat->id, $cat->name,[ 'class' => 'form-check-label']) !!}
                  
                </div>  
              @endforeach             
            </div>
            
            <a href="javascript:void(0)" onclick="show_new_cat()">{{ __('Add Category') }}</a>
            <div class="new-category" style="display:none">
              <div class="form-group">
                
                <select class="form-control" id="parent_cat">
                  <option value="0"> {{ __('No Parent') }}</option>
                  @foreach ($categories as $item)
                  <option value="{{ $item->id }}"> {{ $item->name }}</option>
                  @endforeach
                </select>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" id="new_cat_name" placeholder="">
            </div>
            <input class="btn btn-primary save_cat" type="button" value="Add">
            <input class="btn btn-warning cancel_cat" type="button" value="Cancel">
            </div>
        </div>
        <div class="tags">
            <h5>{{ __('Tags') }}</h5>
        <div class="form-inline">
            <div class="form-group mb-2">
                <input type="text" class="form-control" id="new_tags" placeholder="">
              </div>
              <input class="btn btn-primary save_tags mx-sm-2 mb-2" type="button" value="Add">
              <p><i>{{ __('Separate tags with commas') }}</i></p>
        </div>
        <div class="tag_list">
            <ul class="tag_post_list">
                
                @foreach ($hasTags as $tag)
                <li>{{ $tag->name }}<span><i class="fas fa-times-circle"></i></span><input type="hidden" name="tags[]" value="{{ $tag->id }}"></li> 
                @endforeach
                
                
            </ul>
        </div>
        <div class="unused_tags">
            <a href="javascript:void(0)" class="choose_tags">{{ __('Choose Tags') }}</a>
            <div class="unused_tag_list" style="display:none;">
                <ul>
                    @foreach($tags as $key =>$tag)
                    <li><a href="javascript:void(0)" class="add_to_tag" data-id="{{ $tag->id }}" data-name="{{ $tag->name }}">{{ $tag->name }}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
        </div>
 
          
        
        <div class="form-group">
            {!! Upload::go_upload('feature_image','Feature Image',get_postmeta($post->id,'faeture_image', true))!!}
        </div>
    </div>
    

</div>
{!! Form::close() !!}
@endsection
