@extends('layouts.master')
@section('title'){{ __('Google Setting') }} @endsection
@section('content')
<h2 class="mt-4">{{ __('Google Setting') }}</h2>
<ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="{{route('admin.admin_home')}}">{{ __('Dashboard') }}</a></li>
                            <li class="breadcrumb-item active">{{ __('Google Setting') }}</li>
                        </ol>
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                            <p>{{ $message }}</p>
                            </div>
                            @endif
                        {!! Form::open(array('route' => 'admin.setting.googleUpdate','method'=>'POST','files'=> true))  !!}
                        <div class="row">
                        
                        
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>{{ __('Recaptcha Sitekey') }}</strong>
                                {!! Form::text('sitekey', Setting::get('recaptcha_sitekey'), array('placeholder' => '','class' => 'form-control')) !!}
                                
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>{{ __('Recaptcha Secretkey') }}</strong>
                                {!! Form::text('secretkey', Setting::get('recaptcha_secretkey'), array('placeholder' => '','class' => 'form-control')) !!}
                                
                            </div>
                        </div>
                        
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>{{ __('MAP API Key') }}</strong>
                                {!! Form::text('map_api_key', Setting::get('google_map_api_key'), array('placeholder' => '','class' => 'form-control')) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>{{ __('Google Analytics') }}</strong>
                                {!! Form::textarea('google_analytics', Setting::get('google_analytics'), ['id'=>'google_analytics']) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>{{ __('Google Tag Manager Head Script') }}</strong>
                                {!! Form::textarea('google_tag_head', Setting::get('google_tag_head'), ['id'=>'google_tag_head']) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>{{ __('Google Tag Manager Body Script') }}</strong>
                                {!! Form::textarea('google_tag_body', Setting::get('google_tag_body'), ['id'=>'google_tag_body']) !!}
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <strong></strong>
                                <button type="submit" class="btn btn-primary">{{  __('Submit')}}</button>
                            </div>
                        </div>
                        {!! Form::close() !!} 


@endsection                                                