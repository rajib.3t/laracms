@extends('layouts.master')
@section('title'){{ __('Contact Setting') }} @endsection
@section('content')
<h2 class="mt-4">{{ __('Contact Setting') }}</h2>
<ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="{{route('admin.admin_home')}}">{{ __('Dashboard') }}</a></li>
                            <li class="breadcrumb-item active">{{ __('Contact Setting') }}</li>
                        </ol>
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                            <p>{{ $message }}</p>
                            </div>
                            @endif
                        {!! Form::open(array('route' => 'admin.setting.updateContact','method'=>'POST','files'=> true))  !!}
                        <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>{{ __('Email*') }}</strong>
                                {!! Form::text('email', Setting::get('site_email'), array('placeholder' => 'Email','class' => 'form-control')) !!}
                                <div class="error">
                                    @if (count($errors) > 0)
                                        @foreach ($errors->get('email') as $error)
                                            <p>{{ $error }}</p>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>{{ __('Phone*') }}</strong>
                                {!! Form::text('phone', Setting::get('site_phone'), array('placeholder' => 'Phone','class' => 'form-control')) !!}
                                <div class="error">
                                    @if (count($errors) > 0)
                                        @foreach ($errors->get('phone') as $error)
                                            <p>{{ $error }}</p>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>{{ __('Address 1*') }}</strong>
                                {!! Form::text('address_1', Setting::get('address_1'), array('placeholder' => '','class' => 'form-control')) !!}
                                <div class="error">
                                    @if (count($errors) > 0)
                                        @foreach ($errors->get('address_1') as $error)
                                            <p>{{ $error }}</p>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>{{ __('Address 2') }}</strong>
                                {!! Form::text('address_2', Setting::get('address_2'), array('placeholder' => '','class' => 'form-control')) !!}
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>{{ __('City*') }}</strong>
                                {!! Form::text('city', Setting::get('city'), array('placeholder' => '','class' => 'form-control')) !!}
                                <div class="error">
                                    @if (count($errors) > 0)
                                        @foreach ($errors->get('city') as $error)
                                            <p>{{ $error }}</p>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>{{ __('State*') }}</strong>
                                {!! Form::text('state', Setting::get('state'), array('placeholder' => '','class' => 'form-control')) !!}
                                <div class="error">
                                    @if (count($errors) > 0)
                                        @foreach ($errors->get('state') as $error)
                                            <p>{{ $error }}</p>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>{{ __('Country*') }}</strong>
                                {!! Form::text('country', Setting::get('country'), array('placeholder' => '','class' => 'form-control')) !!}
                                <div class="error">
                                    @if (count($errors) > 0)
                                        @foreach ($errors->get('country') as $error)
                                            <p>{{ $error }}</p>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>{{ __('Postcode*') }}</strong>
                                {!! Form::text('postcode', Setting::get('postcode'), array('placeholder' => '','class' => 'form-control')) !!}
                                <div class="error">
                                    @if (count($errors) > 0)
                                        @foreach ($errors->get('postcode') as $error)
                                            <p>{{ $error }}</p>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <strong></strong>
                                <button type="submit" class="btn btn-primary">{{  __('Submit')}}</button>
                            </div>
                        </div>
                        {!! Form::close() !!} 


@endsection                                                