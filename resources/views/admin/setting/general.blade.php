@extends('layouts.master')
@section('title'){{ __('General Setting') }} @endsection
@section('content')
<h2 class="mt-4">{{ __('General Setting') }}</h2>
<ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="{{route('admin.admin_home')}}">{{ __('Dashboard') }}</a></li>
                            <li class="breadcrumb-item active">{{ __('General Setting') }}</li>
                        </ol>
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                            <p>{{ $message }}</p>
                            </div>
                            @endif
                        {!! Form::open(array('route' => 'admin.setting.update','method'=>'POST','files'=> true))  !!}
                        <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>{{ __('Site Name') }}</strong>
                                {!! Form::text('site_name', Setting::get('site_name'), array('placeholder' => 'Site Name','class' => 'form-control')) !!}
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>{{ __('Tag Line') }}</strong>
                                {!! Form::text('tag_line', Setting::get('tag_line'), array('placeholder' => 'Tag Line','class' => 'form-control')) !!}
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>{{ __('Timezone') }}</strong>
                                {!! Form::select('timezone',array_combine(timezone_identifiers_list(),timezone_identifiers_list()),Setting::get('timezone'),array('class'=>'form-control'))!!}
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>{{ __('Language') }}</strong>
                                {!! Form::select('language',Setting::listLocat(),Setting::get('site_lang'),array('class'=>'form-control'))!!}
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>{{ __('Site Logo') }}</strong><br>
                                {!! Upload::go_upload('site_logo','Site Logo',Setting::get('site_logo')) !!}
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>{{ __('Favicon') }}</strong><br>
                                {!! Upload::go_upload('site_favicon','Site Favicon',Setting::get('site_favicon')) !!}
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <strong></strong>
                                <button type="submit" class="btn btn-primary">{{  __('Submit')}}</button>
                            </div>
                        </div>
                        {!! Form::close() !!} 


@endsection                                                