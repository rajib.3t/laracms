@extends('layouts.master')
@section('title'){{ __('Email Setting') }} @endsection
@section('content')
<h2 class="mt-4">{{ __('Email Setting') }}</h2>
<ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="{{route('admin.admin_home')}}">{{ __('Dashboard') }}</a></li>
                            <li class="breadcrumb-item active">{{ __('Email Setting') }}</li>
                        </ol>
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                            <p>{{ $message }}</p>
                            </div>
                            @endif
                        {!! Form::open(array('route' => 'admin.setting.emailUpdate','method'=>'POST','files'=> true))  !!}
                        <div class="row">
                        
                        
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>{{ __('Emails*') }}</strong>
                                {!! Form::text('emails', Setting::get('send_to_emails'), array('placeholder' => '','class' => 'form-control')) !!}
                                <span><i>{{ __('Separate emails with commas') }}</i></span>
                                <div class="error">
                                    @if (count($errors) > 0)
                                        @foreach ($errors->get('emails') as $error)
                                            <p>{{ $error }}</p>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>{{ __('CC Emails') }}</strong>
                                {!! Form::text('cc_email', Setting::get('send_cc_emails'), array('placeholder' => '','class' => 'form-control')) !!}
                                <span><i>{{ __('Separate emails with commas') }}</i></span>
                            </div>
                        </div>
                        
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>{{ __('BCC Emails') }}</strong>
                                {!! Form::text('bcc_email', Setting::get('send_bcc_emails'), array('placeholder' => '','class' => 'form-control')) !!}
                                <span><i>{{ __('Separate emails with commas') }}</i></span>
                            </div>
                        </div>
                       
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <strong></strong>
                                <button type="submit" class="btn btn-primary">{{  __('Submit')}}</button>
                            </div>
                        </div>
                        {!! Form::close() !!} 


@endsection                                                