<?php

use Illuminate\Database\Seeder;
use App\Category;
class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category = Category::create([
            'parent'=>0,
            'name'=>'Uncategorized',
            'slug'=>'uncategorized'
        ]);
    }
}
